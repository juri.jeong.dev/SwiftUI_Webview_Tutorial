//
//  SwiftUI_Webview_TutorialApp.swift
//  SwiftUI_Webview_Tutorial
//
//  Created by Juri Jeong on 24/12/2022.
//

import SwiftUI

@main
struct SwiftUI_Webview_TutorialApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
