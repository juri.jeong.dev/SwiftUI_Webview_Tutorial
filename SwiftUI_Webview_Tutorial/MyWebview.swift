//
//  MyWebview.swift
//  SwiftUI_Webview_Tutorial
//
//  Created by Juri Jeong on 24/12/2022.
//

import SwiftUI
import WebKit

// UIKit의 UIView를 사용할 수 있도록 함.
// UIController도 마찬가지로 UIViewContollerRepresentable 하면 됨
struct MyWebview: UIViewRepresentable {
    
    var urlToLoad: String
    
    // UIView 만들기
    func makeUIView(context: Context) -> some WKWebView {
        guard let url = URL(string: self.urlToLoad) else { return WKWebView() }
        
        let webView = WKWebView()
        
        webView.load(URLRequest(url: url))
        
        return webView
    }
    
    // 업데이트 UIView
    func updateUIView(_ uiView: UIViewType, context: UIViewRepresentableContext<MyWebview>) {
        
    }
}

struct MyWebview_Previews: PreviewProvider {
    static var previews: some View {
        MyWebview(urlToLoad: "https://www.naver.com")
    }
}
